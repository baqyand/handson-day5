import java.util.Random;

class VoucherGenerator{
	
	public static void main(String[] args){
		for (int i = 1; i <=10 ; i++){
			System.out.print("Voucher kode yang berlaku : ");
			random();
		}
	}
	public static void random(){
		int leftLimit = 97; // letter 'a'
    		int rightLimit = 122; // letter 'z'
    		int targetStringLength = 10;
    		Random random = new Random();

    		String generatedString = random.ints(leftLimit, rightLimit + 1)
      		.limit(targetStringLength)
      		.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
      		.toString();

		System.out.println(generatedString);
	}
}